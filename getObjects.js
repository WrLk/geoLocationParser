const axios = require('axios');
const fs = require('fs');
let region = process.argv[2];

const arr = require(`./${region}/${region}Routes.json`);

const getCoord = (region, id) => {
  return new Promise((resolve, reject) => {
    axios.get(`http://www.maxikarta.ru/${region}/transport/query/stations?route_id=${id}`)
      .then(response => {
        let stationsArr = [];
        let stations = response.data.stations;
        console.log(stations);
        for (let stat of stations) {
          stationsArr.push({
            name: stat.name,
            lat: stat.lat,
            lon: stat.lon
          });
        }
        return resolve(stationsArr) ;
      })
      .catch(error => {
        return reject(error.message)
      })
  })
}

const start = async (region) => {
  let regionArray = [];
  for (let id of arr) {
    await getCoord(region, id).then((stations) => {
      regionArray.push(stations) ;
      console.log(`num ${id}: ${stations}`);
    })
   
  }
  fs.writeFile(`./${region}/${region}Objects.json`, JSON.stringify(regionArray, undefined, 2), function (err) {
    if (err) throw err;
    console.log('Saved!');
  });
  console.log('Done');
}

start(region);