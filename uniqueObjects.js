const fs = require('fs');
const region = process.argv[2];
const arr = require(`./${region}/${region}Objects.json`);

const result = [];
const map = new Map();
for (let i = 0; i < arr.length; i++){
for (let item of arr[i]) {
    if (!map.has(item.name)) {
        map.set(item.name, true); // set any value to Map
        result.push({
            name: item.name,
            lat: item.lat,
            lon: item.lon
        });
    }
}
}
fs.writeFile(`./${region}/${region}.json`, JSON.stringify(result, undefined, 2), function (err) {
    if (err) throw err;
    console.log('Updated!');
});

console.log(result.length)
